package com.vmteam.zoober;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.vmteam.zoober.adapters.RidesAdapter;
import com.vmteam.zoober.models.Message;
import com.vmteam.zoober.models.Ride;

import java.util.ArrayList;
import java.util.Collections;

public class RidesFragment extends Fragment {
private FloatingActionButton startRideButton;
private RecyclerView ridesRecyclerView;
private RidesAdapter ridesAdapter;
private ArrayList<Ride> rides;
private DatabaseReference databaseReference;
private FirebaseUser authenticatedUser;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_rides, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        databaseReference = FirebaseDatabase.getInstance("https://zoober-81dec-default-rtdb.europe-west1.firebasedatabase.app").getReference("Rides");
        authenticatedUser = FirebaseAuth.getInstance().getCurrentUser();

        startRideButton = view.findViewById(R.id.startRideButton);
        ridesRecyclerView = view.findViewById(R.id.ridesRecyclerView);

        rides = new ArrayList<>();
        ridesAdapter = new RidesAdapter(getContext(), rides);
        ridesRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        ridesRecyclerView.setHasFixedSize(true);
        ridesRecyclerView.setAdapter(ridesAdapter);

        fetchRides();

        startRideButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), CreateRideActivity.class);
                startActivity(intent);
            }
        });

    }

    private void fetchRides(){
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                rides.clear();
                for(DataSnapshot dataSnapshot: snapshot.getChildren()){
                    Ride r = dataSnapshot.getValue(Ride.class);

                    //if(!r.getCustomerUsername().equals(authenticatedUser.getDisplayName()) || !r.getDriverUsername().equals("unknown"))
                        rides.add(new Ride(r));
                }

                Collections.sort(rides);
                ridesAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.d("Database: ", error.getMessage());
            }
        });
    }


}
