package com.vmteam.zoober;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.fragment.app.Fragment;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.vmteam.zoober.models.User;

import org.w3c.dom.Text;

public class ProfileFragment extends Fragment {

    private User loggedUser;
    private ImageView imgProfile;
    private TextView displayName;
    private TextView email;
    private FirebaseUser firebaseUser;

    public ProfileFragment(User lgUsr)
    {
        this.loggedUser=lgUsr;
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        imgProfile = (ImageView)view.findViewById(R.id.profile_icon);
        displayName = (TextView)view.findViewById(R.id.profile_displayName);
        email = (TextView)view.findViewById(R.id.profile_email);

        displayName.setText(loggedUser.getUsername());
        email.setText(loggedUser.getEmail());
        Picasso.get().load(loggedUser.getPhotoUrl())
                .resize(200, 200)
                .into(imgProfile, new Callback() {
                    @Override
                    public void onSuccess() {
                        Bitmap imageBitmap = ((BitmapDrawable) imgProfile.getDrawable()).getBitmap();
                        RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(getResources(), imageBitmap);
                        imageDrawable.setCircular(true);
                        imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getHeight()) / 2.0f);
                        imgProfile.setImageDrawable(imageDrawable);
                    }

                    @Override
                    public void onError(Exception e) {

                    }

                });
        Animation animationImgProfile = AnimationUtils.loadAnimation(imgProfile.getContext(),R.anim.bounce);
        Animation animationDisplayName = AnimationUtils.loadAnimation(displayName.getContext(),R.anim.lefttoright);
        Animation animationEmail = AnimationUtils.loadAnimation(email.getContext(),R.anim.lefttoright);

        imgProfile.startAnimation(animationImgProfile);
        displayName.startAnimation(animationDisplayName);
        email.startAnimation(animationEmail);
    }

}
