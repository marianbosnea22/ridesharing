package com.vmteam.zoober;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.common.util.concurrent.JdkFutureAdapters;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.vmteam.zoober.adapters.MessagesAdapter;
import com.vmteam.zoober.models.Message;
import com.vmteam.zoober.models.User;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Formatter;
import java.util.HashMap;
import java.util.TimeZone;

public class ChatFragment extends Fragment {
private EditText messageEditText;
private Button sendMessageButton;
private RecyclerView messagesRecyclerView;
private DatabaseReference databaseReference;
private FirebaseUser authenticatedUser;
private ArrayList<Message> messages;
private MessagesAdapter messagesAdapter;
private User loggedUser;

    public ChatFragment(User loggedUSER) {

        this.loggedUser = loggedUSER;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_chat, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        messageEditText = view.findViewById(R.id.message_editText);
        sendMessageButton = view.findViewById(R.id.send_button);
        messagesRecyclerView = view.findViewById(R.id.messages_recyclerView);

        databaseReference = FirebaseDatabase.getInstance("https://zoober-81dec-default-rtdb.europe-west1.firebasedatabase.app").getReference("Messages");
        authenticatedUser = FirebaseAuth.getInstance().getCurrentUser();

        messages = new ArrayList<>();
        messagesAdapter = new MessagesAdapter(messages, getContext());
        messagesRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        messagesRecyclerView.setHasFixedSize(true);
        messagesRecyclerView.setAdapter(messagesAdapter);

        fetchMessages();


        sendMessageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(messageEditText.getText().toString() != ""){
                     sendMessage(messageEditText.getText().toString());
                     messageEditText.clearFocus();
                     messageEditText.setText("");
                } else{
                    Toast.makeText(getContext(), "Message cannot be empty! ", Toast.LENGTH_SHORT).show();
                }
            }
        });



    }

    private void fetchMessages(){
        String username = loggedUser.getUsername();

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                messages.clear();
                for(DataSnapshot dataSnapshot: snapshot.getChildren()){
                    Message m = dataSnapshot.getValue(Message.class);
                    if(m.getM_senderName().equals(username))
                        m.setMessage_type(1);
                    else
                        m.setMessage_type(0);

                    messages.add(new Message(m));
                    Log.d("Mesaj: ",  m.getM_senderName() + " " + m.getM_messageText() + " " + m.getM_sendTime() + " "  + m.getMessage_type());
                }

                Collections.sort(messages);
                messagesAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.d("Database: ", error.getMessage());
            }
        });

    }


    private String getCurrentTime(){
        Date date = new Date();
        DateFormat df = new SimpleDateFormat("dd MM yyyy HH:mm:ss");
        df.setTimeZone(TimeZone.getTimeZone("Europe/Bucharest"));

        return df.format(date);
    }

    private String buildMessageId(){
    return loggedUser.getUsername().toString().replaceAll(" ", "") + (int)(Calendar.getInstance().getTimeInMillis()/2000);
    }

    private void sendMessage(String message){
        Message messageToSend = new Message();

        messageToSend.setM_messageText(message);
        messageToSend.setM_senderName(loggedUser.getUsername());
        messageToSend.setM_senderPhotoUrl(loggedUser.getPhotoUrl().toString());
        messageToSend.setM_sendTime(getCurrentTime());
        databaseReference.child(buildMessageId()).setValue(messageToSend);

    }



}
