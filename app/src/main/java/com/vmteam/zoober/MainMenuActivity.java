package com.vmteam.zoober;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.vmteam.zoober.models.User;

public class MainMenuActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private GoogleSignInAccount signedInAccount;
    private NavigationView navigationView;
    private Toolbar toolbar;
    private DrawerLayout drawer;
    private User loggedUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        signedInAccount = GoogleSignIn.getLastSignedInAccount(this);
        if(signedInAccount!=null)
        {
            loggedUser=new User(signedInAccount.getDisplayName(),
                    signedInAccount.getEmail(),
                    signedInAccount.getPhotoUrl().toString());
        }
        else
        {
            loggedUser = new User("Vlad Bocancea",
                    "vladbocancea69@smecherie.com",
                    "https://lh3.googleusercontent.com/a-/AOh14GjiyT5--1wbJh3hl3zeDIL4VJfoJQefRKj63j1u7Q=s96-c");
        }
        setupNavigationHeader();



        drawer = findViewById(R.id.drawer_layout);

        setupActionBar();

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                new RidesFragment()).commit();

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.nav_rides:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new RidesFragment()).commit();

                drawer.closeDrawers();
                break;
            case R.id.nav_chat:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new ChatFragment(loggedUser)).commit();
                drawer.closeDrawers();
                break;

            case R.id.nav_profile:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new ProfileFragment(loggedUser)).commit();
                drawer.closeDrawers();
                break;
            case R.id.nav_rides_history:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new RidesHistoryFragment()).commit();
                drawer.closeDrawers();
                break;

            case R.id.nav_logout:
                FirebaseAuth.getInstance().signOut();
                intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                drawer.closeDrawers();
                break;
        }
        return true;
    }


    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }

    }

    private void setupActionBar() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);

        drawer.addDrawerListener(toggle);
        toggle.syncState();

    }

    private void setupNavigationHeader() {
        View navDrawerHeader = navigationView.getHeaderView(0);

        ImageView navDrawerHeaderIcon = (ImageView) navDrawerHeader.findViewById(R.id.nav_header_icon);
        TextView navDrawerHeaderName = (TextView) navDrawerHeader.findViewById(R.id.nav_header_name);
        TextView navDrawerHeaderEmail = (TextView) navDrawerHeader.findViewById(R.id.nav_header_email);

        //Picasso.get().load(signedInAccount.getPhotoUrl()).into(navDrawerHeaderIcon);

        Picasso.get().load(loggedUser.getPhotoUrl())
                .resize(200, 200)
                .into(navDrawerHeaderIcon, new Callback() {
                    @Override
                    public void onSuccess() {
                        Bitmap imageBitmap = ((BitmapDrawable) navDrawerHeaderIcon.getDrawable()).getBitmap();
                        RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(getResources(), imageBitmap);
                        imageDrawable.setCircular(true);
                        imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getHeight()) / 2.0f);
                        navDrawerHeaderIcon.setImageDrawable(imageDrawable);
                    }

                    @Override
                    public void onError(Exception e) {

                    }

                });

        navDrawerHeaderName.setText(loggedUser.getUsername());
        navDrawerHeaderEmail.setText(loggedUser.getEmail());


    }


}