package com.vmteam.zoober;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.vmteam.zoober.adapters.RidesAdapter;
import com.vmteam.zoober.adapters.RidesHistoryAdapter;
import com.vmteam.zoober.models.Ride;

import java.util.ArrayList;
import java.util.Collections;

public class RidesHistoryFragment extends Fragment {

    private RecyclerView ridesHistoryRV;
    private RidesHistoryAdapter ridesHistoryAdapter;
    private ArrayList<Ride> rides;
    private DatabaseReference databaseReference;
    private FirebaseUser authenticatedUser;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_rides_history, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        databaseReference = FirebaseDatabase.getInstance("https://zoober-81dec-default-rtdb.europe-west1.firebasedatabase.app").getReference("Rides");
        ridesHistoryRV = view.findViewById(R.id.ride_history_rv);
        authenticatedUser = FirebaseAuth.getInstance().getCurrentUser();

        rides = new ArrayList<>();
        ridesHistoryAdapter = new RidesHistoryAdapter(getContext(), rides);
        ridesHistoryRV.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        ridesHistoryRV.setHasFixedSize(true);
        ridesHistoryRV.setAdapter(ridesHistoryAdapter);

        fetchRides();

        Log.d("Size ", rides.size() + "");
    }

    private void fetchRides(){
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                rides.clear();
                for(DataSnapshot dataSnapshot: snapshot.getChildren()){
                    Ride r = dataSnapshot.getValue(Ride.class);
                  /*  if(r.getCustomerUsername().equals(authenticatedUser.getDisplayName()))
                    {
                        rides.add(new Ride(r));
                    }*/
                    rides.add(new Ride(r));
                }
                Collections.sort(rides);
                ridesHistoryAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.d("Database: ", error.getMessage());
            }
        });
    }

}
