package com.vmteam.zoober.models;

public class User {
    private String username;
    private String photoUrl; //= "https://lh3.googleusercontent.com/a-/AOh14GjiyT5--1wbJh3hl3zeDIL4VJfoJQefRKj63j1u7Q=s96-c";
    private String email;

    public User(){}
    public User(String username,String email,String photoURL)
    {
        this.email=email;
        this.username=username;
        this.photoUrl=photoURL;
    }

    public String getUsername() {
        return username;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public String getEmail() {
        return email;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}
