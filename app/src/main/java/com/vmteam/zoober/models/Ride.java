package com.vmteam.zoober.models;

public class Ride implements Comparable<Ride> {
    private String customerUsername;
    private String driverUsername;
    private double startLat;
    private double startLong;
    private double destLat;
    private double destLong;
    private String time;

    public  Ride(){ }

    public Ride(String username1, String username2, double lat1, double long1, double lat2, double long2, String time){
        this.customerUsername = username1;
        this.driverUsername = username2;
        this.startLat = lat1;
        this.startLong = long1;
        this.destLat = lat2;
        this.destLong = long2;
        this.time = time;
    }

    public Ride(Ride r){
        this.customerUsername = r.customerUsername;
        this.driverUsername = r.driverUsername;
        this.startLat = r.startLat;
        this.startLong = r.startLong;
        this.destLat = r.destLat;
        this.destLong = r.destLong;
        this.time = r.time;

    }


    @Override
    public int compareTo(Ride ride) {
        return this.time.compareTo(ride.time);
    }

    public String getCustomerUsername() {
        return customerUsername;
    }

    public void setCustomerUsername(String customerUsername) {
        this.customerUsername = customerUsername;
    }

    public String getDriverUsername() {
        return driverUsername;
    }

    public void setDriverUsername(String driverUsername) {
        this.driverUsername = driverUsername;
    }

    public double getStartLat() {
        return startLat;
    }

    public void setStartLat(double startLat) {
        this.startLat = startLat;
    }

    public double getStartLong() {
        return startLong;
    }

    public void setStartLong(double startLong) {
        this.startLong = startLong;
    }

    public double getDestLat() {
        return destLat;
    }

    public void setDestLat(double destLat) {
        this.destLat = destLat;
    }

    public double getDestLong() {
        return destLong;
    }

    public void setDestLong(double destLong) {
        this.destLong = destLong;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
