package com.vmteam.zoober.models;

public class Message implements Comparable<Message> {
    private String m_messageText;
    private String m_senderName;
    private String m_senderPhotoUrl;
    private String m_sendTime;
    private int message_type;

    public Message() { }

    public Message(Message m_mesg)
    {
        this.m_messageText = m_mesg.m_messageText;
        this.m_senderName = m_mesg.m_senderName;
        this.m_senderPhotoUrl = m_mesg.m_senderPhotoUrl;
        this.m_sendTime = m_mesg.m_sendTime;
        this.message_type = m_mesg.message_type;
    }


    public Message(String m_messageText, String m_senderName, String m_senderPhotoUrl, String m_sendTime){
        this.m_messageText = m_messageText;
        this.m_senderName = m_senderName;
        this.m_senderPhotoUrl = m_senderPhotoUrl;
        this.m_sendTime = m_sendTime;
    }


    public String getM_messageText() {
        return m_messageText;
    }

    public void setM_messageText(String m_messageText) {
        this.m_messageText = m_messageText;
    }

    public String getM_senderName() {
        return m_senderName;
    }

    public void setM_senderName(String m_senderName) {
        this.m_senderName = m_senderName;
    }

    public String getM_senderPhotoUrl() {
        return m_senderPhotoUrl;
    }

    public void setM_senderPhotoUrl(String m_senderPhotoUrl) {
        this.m_senderPhotoUrl = m_senderPhotoUrl;
    }

    public String getM_sendTime() {
        return m_sendTime;
    }

    public void setM_sendTime(String m_sendTime) {
        this.m_sendTime = m_sendTime;
    }

    public int getMessage_type() {
        return message_type;
    }

    public void setMessage_type(int message_type) {
        this.message_type = message_type;
    }

    @Override
    public int compareTo(Message message) {
        return this.getM_sendTime().compareTo(message.getM_sendTime());
    }
}
