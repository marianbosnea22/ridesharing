package com.vmteam.zoober;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.vmteam.zoober.models.Ride;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class CreateRideActivity extends AppCompatActivity implements View.OnClickListener {
private TextView startLocationTextView;
private TextView destinationLocationTextView;

private Button startLocationButton;
private Button destinationLocationButton;
private Button createRideButton;

FusedLocationProviderClient fusedLocationProviderClient;

private DatabaseReference databaseReference;
private FirebaseUser authenticatedUser;

private double startLat;
private double startLong;
private double destLat;
private double destLong;


final int PLACE_PICKER_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_ride);

        startLocationTextView = findViewById(R.id.startAddressTextView);
        destinationLocationTextView = findViewById(R.id.destinationAdressTextView);
        startLocationButton = findViewById(R.id.startLocationButton);
        destinationLocationButton = findViewById(R.id.destinationLocationButton);
        createRideButton = findViewById(R.id.mainMenuButton);

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        databaseReference = FirebaseDatabase.getInstance("https://zoober-81dec-default-rtdb.europe-west1.firebasedatabase.app").getReference("Rides");
        authenticatedUser = FirebaseAuth.getInstance().getCurrentUser();

        startLat = -1;
        startLong = -1;
        destLat = -1;
        destLong = -1;


        startLocationButton.setOnClickListener(this);
        destinationLocationButton.setOnClickListener(this);
        createRideButton.setOnClickListener(this);


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PLACE_PICKER_REQUEST){
            if(resultCode == RESULT_OK){
                Place place = PlacePicker.getPlace(data, this);
                StringBuilder stringBuilder = new StringBuilder();
                String latitude = String.valueOf(place.getLatLng().latitude);
                String longitude = String.valueOf(place.getLatLng().longitude);

                List<Address> adresses = getAddressesFromCoordinates(place.getLatLng().latitude, place.getLatLng().longitude);

                destLat = place.getLatLng().latitude;
                destLong = place.getLatLng().longitude;

                stringBuilder.append(adresses.get(0).getAddressLine(0));
                stringBuilder.append(" ");
                stringBuilder.append(adresses.get(0).getLocality());

                destinationLocationTextView.setText(stringBuilder.toString());
            }
        }


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.startLocationButton:
                getStartLocation();

                break;
            case R.id.destinationLocationButton:
                 selectDestinationLocation();
                break;

            case R.id.mainMenuButton:
                insertRideToDatabase();
                Intent intent = new Intent(this, MainMenuActivity.class);
                startActivity(intent);
                break;
        }
    }

    private void getStartLocation(){
        if(ActivityCompat.checkSelfPermission(CreateRideActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
              fusedLocationProviderClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                  @Override
                  public void onComplete(@NonNull Task<Location> task) {
                      Location currentLocation = task.getResult();

                      if(currentLocation!=null){
                          List<Address> adresses = getAddressesFromCoordinates(currentLocation.getLatitude(), currentLocation.getLongitude());
                          StringBuilder stringBuilder = new StringBuilder();

                          startLat = currentLocation.getLatitude();
                          startLong = currentLocation.getLongitude();


                          stringBuilder.append(adresses.get(0).getAddressLine(0));
                          stringBuilder.append(" ");
                          stringBuilder.append(adresses.get(0).getLocality());

                          startLocationTextView.setText(stringBuilder.toString());

                      }
                  }
              });
        } else{
            ActivityCompat.requestPermissions(CreateRideActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 44);
        }
    }

    private void selectDestinationLocation() {
        Toast.makeText(this, "Clicked start location", Toast.LENGTH_SHORT).show();
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(CreateRideActivity.this), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }

    }

    private void insertRideToDatabase(){
       Ride r = new Ride();

       r.setCustomerUsername(authenticatedUser.getDisplayName());
       r.setDriverUsername("unknown");
       r.setStartLat(startLat);
       r.setStartLong(startLong);
       r.setDestLat(destLat);
       r.setDestLong(destLong);
       r.setTime(getCurrentTime());

       databaseReference.child(buildRideId()).setValue(r);
    }

    private String buildRideId(){
        return authenticatedUser.getDisplayName().toString().replaceAll(" ", "") + (int)(Calendar.getInstance().getTimeInMillis()/2000);
    }

    private String getCurrentTime(){
        Date date = new Date();
        DateFormat df = new SimpleDateFormat("dd MM yyyy HH:mm");
        df.setTimeZone(TimeZone.getTimeZone("Europe/Bucharest"));

        return df.format(date);
    }

    private List<Address> getAddressesFromCoordinates(double latitude, double longitude){
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            return geocoder.getFromLocation(latitude, longitude, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }


}