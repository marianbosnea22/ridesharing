package com.vmteam.zoober.adapters;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.vmteam.zoober.R;
import com.vmteam.zoober.models.Ride;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class RidesHistoryAdapter extends RecyclerView.Adapter<RidesHistoryAdapter.ViewHolder>{

    private ArrayList<Ride> rides;
    private LayoutInflater inflater;
    private FirebaseUser authenticatedUser;
    private DatabaseReference databaseReference;
    private Context context;

    public RidesHistoryAdapter(Context context, ArrayList<Ride> data) {
        this.context =context;
        this.inflater = LayoutInflater.from(context);
        this.rides = data;
        authenticatedUser = FirebaseAuth.getInstance().getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance("https://zoober-81dec-default-rtdb.europe-west1.firebasedatabase.app").getReference("Rides");
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.ride_history_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RidesHistoryAdapter.ViewHolder holder, int i) {
        holder.driverUsernameTextView.setText(rides.get(i).getDriverUsername());
        holder.startLocationTextView.setText(buildAdress(rides.get(i).getStartLat(), rides.get(i).getStartLong()));
        holder.destLocationTextView.setText(buildAdress(rides.get(i).getDestLat(), rides.get(i).getDestLong()));
        holder.timeTextView.setText(rides.get(i).getTime());
    }

    @Override
    public int getItemCount() {
        return rides.size();
    }

    private String buildAdress(double lat, double lon){
        List<Address> adresses = getAddressesFromCoordinates(lat, lon);
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(adresses.get(0).getAddressLine(0));
        stringBuilder.append(" ");
        stringBuilder.append(adresses.get(0).getLocality());

        return stringBuilder.toString();
    }

    private List<Address> getAddressesFromCoordinates(double latitude, double longitude){
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            return geocoder.getFromLocation(latitude, longitude, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView driverUsernameTextView;
        TextView startLocationTextView;
        TextView destLocationTextView;
        TextView timeTextView;

        ViewHolder(View itemView) {
            super(itemView);
            driverUsernameTextView = itemView.findViewById(R.id.driverCommandTextView);
            startLocationTextView = itemView.findViewById(R.id.startAddressCommandTextView);
            destLocationTextView = itemView.findViewById(R.id.destinationAdressCommandTextView);
            timeTextView = itemView.findViewById(R.id.dateCommandTextView);

        }
    }

}
