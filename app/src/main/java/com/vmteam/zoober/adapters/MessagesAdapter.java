package com.vmteam.zoober.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.vmteam.zoober.R;
import com.vmteam.zoober.models.Message;

import java.util.ArrayList;

public class MessagesAdapter extends RecyclerView.Adapter{

    private ArrayList<Message> messages;
    private Context context;

    public MessagesAdapter(ArrayList<Message> messages, Context context){
        this.messages = messages;
        this.context = context;
    }



    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view;

        if(viewType == 0){
          view = layoutInflater.inflate(R.layout.others_messages_item, parent, false);
          return new OthersMessageViewHolder(view);
        }
            view = layoutInflater.inflate(R.layout.own_messages_item, parent, false);
        return new OwnMessageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
      if(messages.get(position).getMessage_type() == 0){
          OthersMessageViewHolder viewHolder = (OthersMessageViewHolder) holder;
          viewHolder.messageTextView.setText(messages.get(position).getM_messageText());
          viewHolder.sendTimeTextView.setText(messages.get(position).getM_sendTime().substring(messages.get(position).getM_sendTime().length()-5));


          Picasso.get().load(messages.get(position).getM_senderPhotoUrl())
                  .resize(40, 40)
                  .into(viewHolder.senderIconImageView, new Callback() {
                      @Override
                      public void onSuccess() {
                          Bitmap imageBitmap = ((BitmapDrawable) viewHolder.senderIconImageView.getDrawable()).getBitmap();
                          RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(context.getResources(), imageBitmap);
                          imageDrawable.setCircular(true);
                          imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getHeight()) / 2.0f);
                          viewHolder.senderIconImageView.setImageDrawable(imageDrawable);
                      }

                      @Override
                      public void onError(Exception e) {

                      }

                  });
      } else{
          OwnMessageViewHolder viewHolder = (OwnMessageViewHolder) holder;
          viewHolder.messageTextView.setText(messages.get(position).getM_messageText());
          viewHolder.sendTimeTextView.setText(messages.get(position).getM_sendTime().substring(messages.get(position).getM_sendTime().length() - 8, messages.get(position).getM_sendTime().length() - 3));


          Picasso.get().load(messages.get(position).getM_senderPhotoUrl())
                  .resize(40, 40)
                  .into(viewHolder.senderIconImageView, new Callback() {
                      @Override
                      public void onSuccess() {
                          Bitmap imageBitmap = ((BitmapDrawable) viewHolder.senderIconImageView.getDrawable()).getBitmap();
                          RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(context.getResources(), imageBitmap);
                          imageDrawable.setCircular(true);
                          imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getHeight()) / 2.0f);
                          viewHolder.senderIconImageView.setImageDrawable(imageDrawable);
                      }

                      @Override
                      public void onError(Exception e) {

                      }

                  });
      }
    }

    @Override
    public int getItemViewType(int position) {
        return messages.get(position).getMessage_type();
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }



    class OthersMessageViewHolder extends RecyclerView.ViewHolder{
        TextView messageTextView;
        ImageView senderIconImageView;
        TextView sendTimeTextView;

        public OthersMessageViewHolder(@NonNull View itemView){
            super(itemView);

            messageTextView = itemView.findViewById(R.id.others_message_textView);
            senderIconImageView = itemView.findViewById(R.id.other_sender_icon);
            sendTimeTextView = itemView.findViewById(R.id.others_send_time);
        }


    }

    class OwnMessageViewHolder extends RecyclerView.ViewHolder{
        TextView messageTextView;
        ImageView senderIconImageView;
        TextView sendTimeTextView;

        public OwnMessageViewHolder(@NonNull View itemView){
            super(itemView);
            messageTextView = itemView.findViewById(R.id.own_message_textView);
            senderIconImageView = itemView.findViewById(R.id.own_sender_icon);
            sendTimeTextView = itemView.findViewById(R.id.own_send_time);
        }
    }
}
